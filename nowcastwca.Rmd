---
title: "Comparing nowcast and observed refugees and asylum-seekers statistics in West and Central Africa"
author: "DIMA RB West and Central Africa"
output: rmarkdown::html_document
---

```{r setup, include = FALSE}
library(tidyverse)
library(readxl)
library(janitor)
library(lubridate)
library(unhcrthemes)
library(ggtext)
knitr::opts_chunk$set(echo = FALSE,
                      warning = FALSE,
                      message = FALSE,
                      dev = "svg")
options(htmltools.dir.version = FALSE)
```


# Comparing refugees figures

## Data source

The nowcast will be compared to the refugees and asylum-seekers statistics that we receive from each operation each month. These monthly statistics are extracted from proGres then curated before being shared.

## Indicator used for the comparison

Data from the nowcast model will be compared to observed monthly refugees data using the Mean Absolute Percentage Error (`MAPE`).

$$
MAPE = \frac{1}{n}\sum_{t=1}^n\left|\dfrac{obs - pred}{obs}\right|
$$

It measures the accuracy of a prediction as a percentage. A `MAPE` of 10, means that on average the prediction are off by 10% but we should always examine the plot to make sure that the value is not large because observed values close to 0.

## Plot of the two series

```{r data_clean}
monthly_stat <- read_csv("data/wca_monthly_stat.csv")
nowcast <- read_csv("data/wca_nowcast.csv")
cmpr_rows <- bind_rows(monthly_stat, nowcast)

monthly_stat_agg <- monthly_stat |>
  group_by(date, country_of_asylum) |>
  summarise(refugees_obs = sum(refugees, na.rm = TRUE),
            asylum_seekers_obs = sum(asylum_seekers, na.rm = TRUE)) |>
  ungroup()
nowcast_agg <- nowcast |>
  group_by(date, country_of_asylum) |>
  summarise(refugees_pred = sum(refugees, na.rm = TRUE),
            asylum_seekers_pred = sum(asylum_seekers, na.rm = TRUE)) |>
  ungroup()
cmpr_cols <- full_join(monthly_stat_agg, nowcast_agg)

unhcr_blue <- unhcr_pal(n = 5, "pal_blue")

mape <- function(obs, pred, na.rm = TRUE)
  mean(abs((obs - pred)/pred * 100), na.rm = na.rm)

smape <- function(obs, pred, na.rm = TRUE)
  mean(2 * abs(obs - pred) / (abs(obs) + abs(pred)))
```


```{r chart_ref, fig.height = 7, fig.width = 11}
cmpr_rows |>
  group_by(type, date, country_of_asylum) |>
  summarise(refugees = sum(refugees, na.rm = TRUE),
            asylum_seekers = sum(asylum_seekers, na.rm = TRUE)) |>
  ggplot(aes(date, refugees, color = type)) +
  geom_point(size = 0.75) +
  geom_line() +
  scale_x_date(date_breaks = "1 month", labels = scales::date_format("%b")) +
  scale_y_continuous(limits = c(0, NA),
                     labels = scales::comma) +
  scale_color_unhcr_d() +
  labs(x = "",
       y = "",
       title = "Number of refugees in West and Central Africa",
       subtitle = "Comparison between <span style='color:#0072BC'><strong>nowcasting</strong></span> and <span style='color:#8EBEFF'><strong>observed</strong></span> data",
       caption = "Source: proGres") +
  facet_wrap(vars(country_of_asylum), scales = "free_y") +
  theme_unhcr(grid = "Y") +
  theme(plot.title = element_text(size = 16),
        plot.subtitle = element_markdown(size = 14),
        axis.text.x = element_text(size = 6),
        legend.position = "none")
```

## Accuracy of the nowcast

```{r chart3, fig.width = 9}
cmpr_cols_agg <- cmpr_cols |>
  group_by(country_of_asylum) |>
  summarise(mape_ref = mape(refugees_obs,
                            refugees_pred),
            mape_asy = mape(asylum_seekers_obs,
                            asylum_seekers_pred))


cmpr_cols_agg |>
  ggplot(aes(mape_ref, reorder(country_of_asylum, -mape_ref))) +
  geom_col(fill = unhcr_blue[4]) +
  geom_text(aes(label = paste0(round(mape_ref, 2), "%")),
            hjust = -0.15) +
  scale_x_continuous(expand = expansion(mult = c(0, 0.1))) +
  labs(x = "",
       y = "",
       title = "Mean Absolute Percentage Error Nowcasting vs Observed Asylum-seekers figures",
       subtitle = "Higher values mean larger differences between the nowcasting and the observation",
       caption = "Source: proGres") +
  theme_unhcr(font_size = 12,
              grid = FALSE,
              axis = FALSE) +
  theme(axis.text.x = element_blank())
```

The country with highest `MAPE` is Guinea-Bissau  (`r paste0(round(cmpr_cols_agg$mape_asy[cmpr_cols_agg$country_of_asylum == "Guinea-Bissau"], 2), "%")`) and we can see that the largest difference between estimate and observed data is in January 2021.

Mali and Niger nowcasts are systematically underestimated whereas Liberia and Gabon's are overestimated since January 2021

There are few errors in the observed refugees data, on in February 2021 for Benin and the other for Cote d'Ivoire in August 2021.

# Comparing Asylum-seekers figures


## Plot of the two series


```{r chart2, fig.height = 6.5, fig.width = 10.5}
cmpr_rows |>
  group_by(type, date, country_of_asylum) |>
  summarise(refugees = sum(refugees, na.rm = TRUE),
            asylum_seekers = sum(asylum_seekers, na.rm = TRUE)) |>
  ggplot(aes(date, asylum_seekers, color = type)) +
  geom_point(size = 0.75) +
  geom_line() +
  scale_x_date(date_breaks = "1 month", labels = scales::date_format("%b")) +
  scale_y_continuous(limits = c(0, NA),
                     labels = scales::comma) +
  scale_color_unhcr_d() +
  labs(x = "",
       y = "",
       title = "Number of asylum-seekers in West and Central Africa",
       subtitle = "Comparison between <span style='color:#0072BC'><strong>nowcasting</strong></span> and <span style='color:#8EBEFF'><strong>observed</strong></span> data",
       caption = "Source: proGres") +
  facet_wrap(vars(country_of_asylum), scales = "free_y") +
  theme_unhcr(grid = "Y") +
  theme(plot.title = element_text(size = 16),
        plot.subtitle = element_markdown(size = 14),
        axis.text.x = element_text(size = 6),
        legend.position = "none")
```

## Accuracy of the nowcast

```{r chart4, fig.width = 9}
cmpr_cols_agg |>
  ggplot(aes(mape_asy, reorder(country_of_asylum, -mape_asy))) +
  geom_col(fill = unhcr_blue[4]) +
  geom_text(aes(label = paste0(round(mape_asy, 2), "%")),
            hjust = -0.15) +
  scale_x_continuous(expand = expansion(mult = c(0, 0.2))) +
  labs(x = "",
       y = "",
       title = "Mean Absolute Percentage Error Nowcasting vs Observed Asylum-seekers figures",
       subtitle = "Higher values mean larger differences between the nowcasting and the observation",
       caption = "Source: proGres") +
  theme_unhcr(font_size = 12,
              grid = FALSE,
              axis = FALSE) +
  theme(axis.text.x = element_blank())
```

Guinea is the country with highest `MAPE` and this discrepencies can be explained by the fact that asylum-seekers data are managed by the government and is not up-to-date on proGres v4.

In Niger, the `MAPE` is also large (`r paste0(round(cmpr_cols_agg$mape_asy[cmpr_cols_agg$country_of_asylum == "Niger"], 2), "%")`) and looking at the first graph, both series started to diverge after January. Looking at the nowcast, it looks like from February onward, a large population (~ 35k) of Nigeria asylum-seekers were considered. These population (at this magnitude) are not in the registry.

Nigeria and Gabon both have a `MAPE` around (`r paste0(round(cmpr_cols_agg$mape_asy[cmpr_cols_agg$country_of_asylum == "Nigeria"]), "%")`). For Nigeria, the nowcast show similar trends as the observation with a fixed shift of 1k on average. For Gabon, between January and August there's relative large difference between estimate and observed values.
